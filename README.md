# S3 MIRROR

A simple Docker container to mirror a S3 storage.

Internally, the container is running a MinIO Storage server, with S3 API on internal port 9000 and MinIO console on port 9001.

To sync data, a crontab is created for the buckets to sync.

## Running

`docker run --rm -v /path/to/local/s3-data:/data -p 9000:9000 -p 9001:9001 --env-file .env --name s3-mirror registry.gitlab.com/ousamg/s3-mirror:latest`

## Environment file

An environment file should be passed to the Docker container, with the following supported variables:

| Name                     | Required? | Allowed Values | Default                       | Description                                                                                                                                       |
| ------------------------ | --------- | -------------- | ----------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------- |
| `S3_URL`                 | Yes       |                |                               | URL to remote S3 storage                                                                                                                          |
| `S3_SECRET_KEY`          | Yes       |                |                               | Remote S3 secret key                                                                                                                              |
| `S3_ACCESS_KEY`          | Yes       |                |                               | Remote S3 access key                                                                                                                              |
| `S3_LOCAL_ACCESS_KEY`    | No        |                |                               | Access key for local MinIO object storage. If not provided, one will be created on initial run (only printed once)                                |
| `S3_LOCAL_SECRET_KEY`    | No        |                |                               | Secret key for local MinIO object storage. If not provided, one will be created on initial run (only printed once)                                |
| `S3_LOCAL_BUCKET_POLICY` | No        |                | Preserve remote bucket policy | Set public access policy of the local bucket(s). See [mc anonymous set](https://min.io/docs/minio/linux/reference/minio-mc/mc-anonymous-set.html) |
| `CRON_EXPRESSION`        | No        |                | \*/5 \* \* \* \*              | Cron expression for syncing remote buckets (default: every 5 minutes)                                                                             |
| `S3_BUCKETS`             | No        |                |                               | Sync only these (space-separated) buckets from the remote. Default behaviour is to sync all buckets                                               |
| `AWS_SIGNATURE_VERSION`  | No        | s3v4,s3v2      | s3v4                          | Protocol for authenticating requests to remote                                                                                                    |
