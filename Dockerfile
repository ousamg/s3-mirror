FROM debian:11

RUN apt update && apt install -y wget cron moreutils

RUN wget https://dl.min.io/server/minio/release/linux-amd64/archive/minio_20230420175655.0.0_amd64.deb -O minio.deb && \
    dpkg -i minio.deb

RUN cd /usr/local/bin && \
    wget https://dl.min.io/client/mc/release/linux-amd64/mc && \
    chmod +x /usr/local/bin/mc

COPY . /app
WORKDIR /app

ENTRYPOINT [ "/bin/bash", "-c", "/app/entrypoint.sh | ts '[%Y-%m-%d %H:%M:%S]'" ]

