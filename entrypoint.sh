#!/bin/bash -ue


minio server /data --console-address ":9001" &
PID=$(echo $!)

# Set up local S3 server
sleep 2
set +e
mc alias set s3_local http://localhost:9000 minioadmin minioadmin
mc alias set s3_remote $S3_URL $S3_ACCESS_KEY $S3_SECRET_KEY --api ${AWS_SIGNATURE_VERSION:-"s3v4"}

if [[ ! -z "${S3_LOCAL_ACCESS_KEY:-}" ]]; then
    echo "Using specified local access key"
    mc admin user svcacct add --access-key $S3_LOCAL_ACCESS_KEY --secret-key ${S3_LOCAL_SECRET_KEY} s3_local minioadmin | sed -e "s/$S3_LOCAL_ACCESS_KEY/\[REDACTED\]/g" -e "s/$S3_LOCAL_SECRET_KEY/\[REDACTED\]/g"
else 
    echo "No local access key specified, generating one"
    mc admin user svcacct add s3_local minioadmin
fi

set -e

# Find buckets to sync
if [[ ! -z "${S3_BUCKETS:-}" ]]; then
    echo "Using specified buckets: ${S3_BUCKETS}"
else 
    echo "No buckets specified, using all buckets"
    S3_BUCKETS=$(mc ls s3_remote | grep -oP "([^\s]+$)")
fi


# Set up crontab to sync buckets
crontab
crontab -l > mycron

for BUCKET_NAME in ${S3_BUCKETS}; do
    # Create bucket if it doesn't exist
    echo "Creating bucket $BUCKET_NAME"
    set +e
    mc mb s3_local/$BUCKET_NAME 
    set -e

    if [[ -z "${S3_LOCAL_BUCKET_POLICY:-}" ]]; then
        echo "No bucket policy specified, skipping"
    else
        echo "Setting bucket policy ${S3_LOCAL_BUCKET_POLICY} for $BUCKET_NAME"
        mc anonymous set ${S3_LOCAL_BUCKET_POLICY} s3_local/${BUCKET_NAME}
    fi

    # Add sync to cron
    echo "Adding sync to cron for $BUCKET_NAME"
    echo "${CRON_EXPRESSION:-"*/5 * * * *"} /app/sync-bucket.sh ${BUCKET_NAME} 1> /proc/1/fd/1 2>/proc/1/fd/2" >> mycron
done

# Install crontab
crontab mycron
rm mycron

echo "Running with following crontab: "
crontab -l

echo "Starting cron..."
cron -f
