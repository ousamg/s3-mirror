#!/bin/bash -ue

BUCKET=$1

if [[ -z "${S3_LOCAL_BUCKET_POLICY:-}" ]]; then
    PRESERVE=""
else
    PRESERVE="--preserve"
fi

flock -n /tmp/sync-${BUCKET/\//}.lock /usr/local/bin/mc mirror --remove --overwrite ${PRESERVE} s3_remote/${BUCKET} s3_local/${BUCKET} | ts "[%Y-%m-%d %H:%M:%S] Syncing $(printf %-20s ${BUCKET}) |"
